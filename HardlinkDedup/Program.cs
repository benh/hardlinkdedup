﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Delimon.Win32.IO;

namespace HardlinkDedup
{
	class Program
	{
		static void Main(string[] args)
		{
			Program p = new Program();
			p.root = String.Join(" ", args);
			p.Exec();
		}

		[System.Runtime.InteropServices.DllImport("kernel32.dll", SetLastError = true)]
		static extern bool GetFileInformationByHandle(IntPtr hFile, out BY_HANDLE_FILE_INFORMATION lpFileInformation);

		string root = @"D:\";

		// excluded files 
		List<string> excludedFiles = new List<string>();
		List<string> excludedTypes = new List<string>();

		// load cache
		public Program()
		{
			
		}

		private void Exec()
		{
			//string[] data = System.IO.File.ReadAllLines("cache.dat");


			List<FileInfo> allFiles = new List<FileInfo>();
			List<DirectoryInfo> dirQueue = new List<DirectoryInfo>();
			foreach (var x in root.Split(';'))
				dirQueue.Add(new DirectoryInfo(x.Trim(new [] { ' ', '"' })));

			string cacheFile = Path.Combine(dirQueue.First().FullName, "cache.db");
			excludedFiles.Add(cacheFile); // don't deal with the cache 

			while (dirQueue.Count > 0)
			{
				var next = dirQueue.Last();
				//if (dirQueue.Count % 100 == 0)
					Console.WriteLine("Scanning " + next.FullName);
				dirQueue.RemoveAt(dirQueue.Count - 1);

				try
				{
					dirQueue.AddRange(next.GetDirectories());
					allFiles.AddRange(next.GetFiles());
				}
				catch (Exception e)
				{
					Console.WriteLine("-> Error: " + e.Message);
				}
			}

			allFiles.RemoveAll(a => a.FullName.Contains(@":\$RECYCLE"));
			allFiles.RemoveAll(a => a.FullName.Contains(@"\WindowsApps\"));

			Console.WriteLine("Found {0} files", allFiles.Count);

			// [refine] 
			Console.WriteLine("Excluding small files");
			allFiles = allFiles.Where(f => f.Length > 4096).ToList();

			// find files with same size - possible duplicates
			Console.WriteLine("Grouping files by size");
			var possibleDupes = allFiles.GroupBy(f => f.Length);

			// [refine] only groups that have more than 1 entry 
			Console.WriteLine("Removing files which have unique size");
			possibleDupes = possibleDupes.Where(g => g.Count() > 1);

			int counter = possibleDupes.Sum(f => f.Count());
			int cache_flush_delta = 1000;
			int cache_flush_counter = cache_flush_delta;
			List<DedupData> loaded_dedup_data = new List<DedupData>(counter + 1);
			List<DedupData> all_dedup_data = new List<DedupData>(counter + 1);

            Console.WriteLine("{0} files left to consider", counter);

			// load cache 
			Console.WriteLine("Loading cache");
			if (File.Exists(cacheFile))
			{
				Console.WriteLine("   Indexing file names");
				var fileMap = allFiles.ToDictionary( key => key.FullName.ToLower() );

				Console.WriteLine("   Matching cache entries to filesystem entries");
				string[] lines = File.ReadAllLines(cacheFile);
				string[][] data = Array.ConvertAll(lines, line => line.Split(new char[] { ' '}, 8));
				lines = null;
				foreach (var line in data.Where(line => line.Length == 8))
				{
					string filename = (root.Substring(0, 1) + line[7]).ToLower();
					if (fileMap.ContainsKey(filename))
					{
						DedupData dd = new DedupData();
						dd.first4KHash = (line[0] == "null") ? null : Convert.FromBase64String(line[0]);
						dd.fullSHA2 = (line[1] == "null") ? null : Convert.FromBase64String(line[1]);
						dd.Length = Convert.ToInt64(line[2]);
						dd.nNumberOfLinks = Convert.ToInt32(line[3]);
						dd.nFileIndexHigh = Convert.ToInt32(line[5]);
						dd.nFileIndexLow  = Convert.ToInt32(line[6]);
						dd.lastModified = Convert.ToInt64(line[4]);
						dd.fi = fileMap[filename];
						if (dd.lastModified == dd.fi.LastWriteTimeUtc.Ticks) // not modified
							loaded_dedup_data.Add(dd);
					}
				}
			}
			Console.WriteLine("-> {0} items", loaded_dedup_data.Count);
			var cacheMap = loaded_dedup_data.ToDictionary(key => key.fi.FullName.ToLower());

			var bat = File.CreateText("c:\\tmp\\output.bat");
			foreach (var group in possibleDupes)
			{
				// analyzing files 
				List<DedupData> ddlist = new List<DedupData>();
				foreach (var file in group)
				{
					if (counter-- % 100 == 0)
						Console.WriteLine("[{0}] {1}", counter, file.Name);

					// check the cache 
					var lcfilename = file.FullName.ToLower();
					if (cacheMap.ContainsKey(lcfilename))
					{
						var cmi = cacheMap[lcfilename];
						ddlist.Add(cmi);
						cacheMap.Remove(lcfilename);
						continue; // file has not changed
					}

					using (var fs = file.OpenRead())
					{
						BY_HANDLE_FILE_INFORMATION fi;
						if (!GetFileInformationByHandle(fs.SafeFileHandle.DangerousGetHandle(), out fi))
						{
							fs.Close(); // close file as soon as possible
							Console.WriteLine(file.FullName);
							Console.WriteLine("-> ERROR " + System.Runtime.InteropServices.Marshal.GetLastWin32Error());
						}
						else
						{
							var b = new byte[4096];
							fs.Read(b, 0, b.Length);
							fs.Close(); // close file as soon as possible

							var dd = new DedupData();
							dd.fi = file;
							dd.nFileIndexHigh = fi.nFileIndexHigh;
							dd.nFileIndexLow = fi.nFileIndexLow;
							dd.nNumberOfLinks = fi.nNumberOfLinks;
							dd.Length = file.Length;
							dd.lastModified = file.LastWriteTimeUtc.Ticks;
							dd.SetFirst4K(b);
							ddlist.Add(dd);
						}
					}
				}
				all_dedup_data.AddRange(ddlist);


				

				// analyzing for duplicates
				// group by 4K hashes
				foreach (var dupgroup in ddlist.GroupBy(f => Convert.ToBase64String(f.first4KHash) ).Where(g => g.Count() > 1))
				{
					List<DedupData> dupes = dupgroup.ToList();
					
					// update link information if needed
					foreach (var cmi in dupes.Where(cc => cc.nFileIndexLow <= 0 || cc.nFileIndexHigh <= 0 || cc.nNumberOfLinks <= 0))
					{
						using (var fstream = cmi.fi.OpenRead())
						{
							BY_HANDLE_FILE_INFORMATION fi;
							if (GetFileInformationByHandle(fstream.SafeFileHandle.DangerousGetHandle(), out fi))
							{
								cmi.nNumberOfLinks = fi.nNumberOfLinks;
								cmi.nFileIndexHigh = fi.nFileIndexHigh;
								cmi.nFileIndexLow = fi.nFileIndexLow;
								Console.WriteLine(cmi.fi.FullName + fi.nNumberOfLinks);
								fstream.Close();
							}
							else
							{
								Console.WriteLine("-> ERROR " + System.Runtime.InteropServices.Marshal.GetLastWin32Error());
								cmi.nNumberOfLinks = 0;
								cmi.nFileIndexHigh = -1;
								cmi.nFileIndexLow = -1;
								fstream.Close();
							}
						}
					}


					// remove hardlink pairs
					for (int i = 0; i < dupes.Count; ++i)
						for (int j = dupes.Count - 1; j > i; --j)
							if (dupes[i].nFileIndexHigh > 0
								 && dupes[i].nFileIndexLow > 0
								 && dupes[i].nFileIndexHigh == dupes[j].nFileIndexHigh
								 && dupes[i].nFileIndexLow == dupes[j].nFileIndexLow)
							{
								dupes.RemoveAt(j); // already a hard link
							}

					foreach (var file in dupes)
					{
						using (var fs = file.fi.OpenRead())
						using (var fs_br = new System.IO.BufferedStream(fs, 16 * 1048576)) // 16 MB
						{
							file.fullSHA2 = sha2.ComputeHash(fs_br);
							fs_br.Close();
							fs.Close();
						}
					}
					
					Console.CancelKeyPress += Console_CancelKeyPress;
					var c = dupes.GroupBy(f => f.fullSHA2, byteArrayEq).OrderByDescending(f => f.First().Length); // group and tackle larger files first
					foreach (var d in c)
					{
						var duplicate = d.ToList();
						if (duplicate.Count > 1 && duplicate.Count < 1000)
						{
							Console.WriteLine("HARD LINKING DUPLICATES:");
							foreach (var di in duplicate)
							{
								//Console.WriteLine("   -> ({0}, {1}) - {2}", di.nFileIndexLow, di.nFileIndexHigh, di.fi.FullName);
								Console.WriteLine("    -> {0}", di.fi.FullName);
							}

							var target = d.First();
							foreach (var link in duplicate.Skip(1))
							{
								string fnExisting = target.fi.FullName;

								string fnFinal = link.fi.FullName;
								string fnTemp = fnFinal + ".tmp";


								//Console.WriteLine("   -> Creating link {0}", Path.GetFileName(fnTemp));
								if (CreateHardLink(fnTemp, target.fi.FullName, IntPtr.Zero) != 0)
								{
									FileInfo fiFinal = new FileInfo(fnFinal);
									DateTime cre = fiFinal.CreationTimeUtc;
									DateTime mod = fiFinal.LastWriteTimeUtc;
									DateTime acc = fiFinal.LastAccessTimeUtc;
									var attrib = fiFinal.Attributes;

									//try
									//{
									//Console.WriteLine("   -> Deleting {0}", fnFinal);
									//File.Delete(fnFinal);
									Microsoft.VisualBasic.FileIO.FileSystem.DeleteFile(fnFinal, Microsoft.VisualBasic.FileIO.UIOption.OnlyErrorDialogs, Microsoft.VisualBasic.FileIO.RecycleOption.SendToRecycleBin);

									//Console.WriteLine("   -> Moving {0} -> {0}", fnTemp, fnFinal);
									File.Move(fnTemp, fnFinal);

									//}
									//catch (Exception x)
									//{
									//    Console.WriteLine("FAILURE!! " + x.Message);
									//    if (File.Exists(fnTemp))
									//        File.Delete(fnTemp);
									//}

									//Console.WriteLine("  -> Resetting original file modification times");
									fiFinal = new FileInfo(fnFinal);
									fiFinal.IsReadOnly = false;
									fiFinal.CreationTimeUtc	  = cre;
									fiFinal.LastWriteTimeUtc  = mod;
									fiFinal.LastAccessTimeUtc = acc;
									fiFinal.Attributes = attrib;

									if (cancelled == 1)
									{
										Console.WriteLine("Cleanly shutdown by CTRL+C");
										return;
									}
								}
								else
								{
									Console.WriteLine("-> ERROR " + System.Runtime.InteropServices.Marshal.GetLastWin32Error());
									//throw new Exception("failed to make hard link");
								}
								

								//bat.WriteLine(String.Format(@"mklink /H ""{0}.ddp-tmp"" ""{1}""", fn, target.fi.FullName));
								//bat.WriteLine(String.Format(@"del ""{0}""", fn));
								//bat.WriteLine(String.Format(@"move ""{0}.ddp-tmp"" ""{0}""", fn));
							}
							bat.WriteLine();
							bat.WriteLine();

							Console.WriteLine();
						}
					}

				}


				// flush the cache if needed
				cache_flush_counter -= ddlist.Count;
				if (cache_flush_counter < 0)
				{
					cache_flush_counter = cache_flush_delta;
					writeCache(cacheFile, all_dedup_data);
				}

			}
		}

		int cancelled = 0;
		void Console_CancelKeyPress(object sender, ConsoleCancelEventArgs e)
		{
			e.Cancel = true;
			cancelled = 1;
		}

		[System.Runtime.InteropServices.DllImport("Kernel32.dll", CharSet = System.Runtime.InteropServices.CharSet.Unicode, SetLastError = true)]
		public static extern int CreateHardLink(
			string lpFileName,
			string lpExistingFileName,
			IntPtr lpSecurityAttributes
		);

		private static ArrayEqualityComparer<byte> byteArrayEq = new ArrayEqualityComparer<byte>();
		private static System.Security.Cryptography.SHA256CryptoServiceProvider sha2 = new System.Security.Cryptography.SHA256CryptoServiceProvider();

		private void writeCache(string cacheFile, List<DedupData> cache)
		{
			StringBuilder sb = new StringBuilder();
			foreach (var ci in cache.OrderBy(f => Convert.ToBase64String(f.first4KHash)))
			{
				string relativePath = ci.fi.FullName.Substring(1);
				string[] entries = new string[8];
				entries[0] = ci.first4KHash == null ? "null" : Convert.ToBase64String(ci.first4KHash);
				entries[1] = ci.fullSHA2 == null ? "null" : Convert.ToBase64String(ci.fullSHA2);
				entries[2] = Convert.ToString(ci.Length);
				entries[3] = Convert.ToString(ci.nNumberOfLinks);
				entries[4] = Convert.ToString(ci.lastModified);
				entries[5] = Convert.ToString(ci.nFileIndexLow);
				entries[6] = Convert.ToString(ci.nFileIndexHigh);
				entries[7] = relativePath;
				sb.AppendLine(String.Join(" ", entries));
			}
			if (File.Exists(cacheFile))
				File.Delete(cacheFile);
			File.WriteAllText(cacheFile, sb.ToString());
			File.SetAttributes(cacheFile, FileAttributes.Compressed | FileAttributes.Archive);
		}

		private void readCache()
		{

		}

	}

	public class DedupData
	{
		private static System.Security.Cryptography.SHA1CryptoServiceProvider sha1 = new System.Security.Cryptography.SHA1CryptoServiceProvider();
		public FileInfo fi;
		public long Length;
		public Int32 nFileIndexHigh;
		public Int32 nFileIndexLow;
		public Int32 nNumberOfLinks;
		public long lastModified;

		public byte[] first4KHash { get; set; }

		public void SetFirst4K(byte[] data)
		{
			first4KHash = sha1.ComputeHash(data);
		}

		public byte[] fullSHA2
		{
			get;
			set;
		}

		public override string ToString()
		{
			return "{" + string.Join(" ", Convert.ToBase64String(first4KHash), Convert.ToBase64String(fullSHA2), fi.FullName) + "}";
		}

	}


	[System.Runtime.InteropServices.StructLayout(System.Runtime.InteropServices.LayoutKind.Sequential)]
	public struct FILETIME
	{
		public Int32 dwLowDateTime;
		public Int32 dwHighDateTime;
	}

	[System.Runtime.InteropServices.StructLayout(System.Runtime.InteropServices.LayoutKind.Sequential)]
	public struct BY_HANDLE_FILE_INFORMATION
	{
		public Int32 dwFileAttributes;
		public FILETIME ftCreationTime;
		public FILETIME ftLastAccessTime;
		public FILETIME ftLastWriteTime;
		public Int32 dwVolumeSerialNumber;
		public Int32 nFileSizeHigh;
		public Int32 nFileSizeLow;
		public Int32 nNumberOfLinks;
		public Int32 nFileIndexHigh;
		public Int32 nFileIndexLow;
	}

}
